$(function(){
	
		//clone div's desktop to menu resposnsive 
		$('.headerList').clone().appendTo('.menu-sidebar-cnt').addClass('menu-responsive').removeClass('headerList');
		$('.headerLogo').clone().prependTo('.menu-sidebar-cnt').removeClass('header_logo').addClass('responsive_logo');
		// $('.header-languages-select').clone().appendTo('.menu-sidebar-cnt');
	
		//events: menu burguer
		function cerrar_nav() {
			$('.menu-mobile-open').removeClass('active');
			$('.menu-mobile-close').removeClass('active');
			$('.menu-sidebar').removeClass('active');
			$('.header-menu-overlay').removeClass('active');
			$('body').removeClass('active');
		};
	
		function abrir_nav(){
			$('.menu-mobile-open').addClass('active');
			$('.menu-mobile-close').addClass('active');
			$('.menu-sidebar').addClass('active');
			$('.header-menu-overlay').addClass('active');
			$('body').addClass('active');
		}
	
		$('.menu-mobile-close , .header-menu-overlay').click(function(event) {
			event.preventDefault();
			cerrar_nav();
		});	
	
		$('.menu-mobile-open').click(function(event) {
			abrir_nav()
		});
	
		//detectando tablet, celular o ipad
		var isMobile = {
			Android: function() {
				return navigator.userAgent.match(/Android/i);
			},
			BlackBerry: function() {
				return navigator.userAgent.match(/BlackBerry/i);
			},
			iOS: function() {
				return navigator.userAgent.match(/iPhone|iPad|iPod/i);
			},
			Opera: function() {
				return navigator.userAgent.match(/Opera Mini/i);
			},
			Windows: function() {
				return navigator.userAgent.match(/IEMobile/i);
			},
			any: function() {
				return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
			}
		};
		
		// dispositivo_movil = $.browser.device = (/android|webos|iphone|ipad|ipod|blackberry|iemobile|opera mini/i.test(navigator.userAgent.toLowerCase()))
		if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
			// tasks to do if it is a Mobile Device
			function readDeviceOrientation() {
				if (Math.abs(window.orientation) === 90) {
					// Landscape
					cerrar_nav();
				} else {
					// Portrait
					cerrar_nav();
				}
			}
			window.onorientationchange = readDeviceOrientation;
		}else{
			$(window).resize(function() {
				var estadomenu = $('.menu-responsive').width();
				if(estadomenu != 0){
					cerrar_nav();
				}
			});
		}

		// Ancla scroll - AGREGAR CLASE DEL ENLACE
		$('.linkScroll a').click(function() {
			cerrar_nav();
			if(location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')&& location.hostname == this.hostname) {
				var $target = $(this.hash);
				$target = $target.length && $target || $('[name=' + this.hash.slice(1) +']');
				if ($target.length) {
				var targetOffset = $target.offset().top-80;
				$('html,body').animate({scrollTop: targetOffset}, 1000);
				return false;
				}
			}
		});
	
		// header scroll
		var altoScroll = 0
		$(window).scroll(function() {
			altoScroll = $(window).scrollTop();
			if (altoScroll > 0) {
				$('.menu-mobile-open').addClass('color');
			}else{
				$('.menu-mobile-open').removeClass('color');
			};
			if (altoScroll > 0) {
				$('.header').addClass('scroll');
			}else{
				$('.header').removeClass('scroll');
			};
		});
	});
	